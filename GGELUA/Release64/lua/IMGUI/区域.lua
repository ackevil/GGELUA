--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 19:04:13
    @LastEditTime : 2021-04-24 19:20:12
--]]
local im = require"gimgui"
local IMBase = require"IMGUI.基类"

local IM区域 = class('IM区域','IMBase')

function IM区域:初始化(name,w,h,border,flag)
    self._n = name
    self._w = w
    self._h = h
    self._b = border
    self._f = flag
end

function IM区域:开始()
    return im.BeginChild(self._n,self._w,self._h,self._b,self._f)
end

function IM区域:结束(x,y)
    if self._auto then
        if im.GetScrollY() >= im.GetScrollMaxY() then
            im.SetScrollHereY(1);
        end
    end
    im.EndChild()
end

function IM区域:自动滚动(b)
    self._auto = b
    return self
end
return IM区域