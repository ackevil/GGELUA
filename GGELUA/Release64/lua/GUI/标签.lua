--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-09 04:04:42
--]]


local _ENV = require("GGE界面")

GUI标签 = class("GUI标签",GUI控件)
GUI标签._type = 13
function GUI标签:初始化()
    
    self._rect = {}
end

function GUI标签:置可见(...)
    GUI控件.置可见(self,...)
    for i,v in ipairs(self.子控件) do
        if v:取类型()=='单选按钮' and v.是否选中 then
            v:置选中(true)
            break
        end
    end
end

function GUI标签:创建区域(btn,x,y,w,h)
    local r = self:创建控件(btn.名称..'区域',x,y,w,h)
    local 置选中 = btn.置选中
    btn.置选中 = function (this,v)--绑定按钮选中事件
        if v then
            for _,v in pairs(self._rect) do
                v:置可见(v==r,true)
            end
            置选中(this,v)
        end
    end
    self._rect[btn] = r
    --把区域放到按钮下面
    local n = #self.子控件
    for i,v in ipairs(self.子控件) do
        if v==btn then
            self.子控件[i],self.子控件[n]=self.子控件[n],self.子控件[i]
            break
        end
    end
    return r
end

return GUI标签