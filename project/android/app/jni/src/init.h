#ifndef init_h
#define init_h
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
int Utf8_Ansi(lua_State *L);
int GetRunPath (lua_State *L);
int gge_init (lua_State *L);
int gge_getscript(lua_State *L);
#endif